package threads.server.core.threads;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Thread.class}, version = 250)
public abstract class ThreadsDatabase extends RoomDatabase {

    public abstract ThreadDao threadDao();

}
