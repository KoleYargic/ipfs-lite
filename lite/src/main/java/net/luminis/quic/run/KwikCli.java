/*
 * Copyright © 2019, 2020, 2021 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.run;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicSessionTicket;
import net.luminis.quic.Version;
import net.luminis.quic.VersionNegotiationFailure;
import net.luminis.quic.log.FileLogger;
import net.luminis.quic.log.Logger;
import net.luminis.quic.log.SysOutLogger;
import net.luminis.quic.stream.QuicStream;
import net.luminis.tls.NewSessionTicket;
import net.luminis.tls.TlsConstants;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * Command line interface for Kwik
 */
public class KwikCli {

    private static final String DEFAULT_LOG_ARGS = "wip";


    private static PrivateKey readKey(String clientKey) throws IOException, InvalidKeySpecException {
        String key = new String(Files.readAllBytes(Paths.get(clientKey)), Charset.defaultCharset());

        if (key.contains("BEGIN PRIVATE KEY")) {
            return loadRSAKey(key);
        } else if (key.contains("BEGIN EC PRIVATE KEY")) {
            throw new IllegalArgumentException("EC private key must be in DER format");
        } else {
            // Assume DER format
            return loadECKey(Files.readAllBytes(Paths.get(clientKey)));
        }
    }

    private static PrivateKey loadRSAKey(String key) throws InvalidKeySpecException {
        String privateKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PRIVATE KEY-----", "");
        byte[] encoded = Base64.getMimeDecoder().decode(privateKeyPEM);

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            return keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing key algorithm RSA");
        }
    }

    private static PrivateKey loadECKey(byte[] pkcs8key) throws InvalidKeySpecException {
        try {
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(pkcs8key);
            KeyFactory factory = KeyFactory.getInstance("EC");
            PrivateKey privateKey = factory.generatePrivate(spec);
            return privateKey;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing ECDSA support");
        }
    }

    private static X509Certificate readCertificate(String certificateFile) throws IOException, CertificateException {
        String fileContent = new String(Files.readAllBytes(Paths.get(certificateFile)), Charset.defaultCharset());

        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        if (fileContent.startsWith("-----BEGIN CERTIFICATE-----")) {
            String encodedCertificate = fileContent
                    .replace("-----BEGIN CERTIFICATE-----", "")
                    .replaceAll(System.lineSeparator(), "")
                    .replace("-----END CERTIFICATE-----", "");
            Certificate certificate = certificateFactory.generateCertificate(new ByteArrayInputStream(Base64.getDecoder().decode(encodedCertificate)));
            return (X509Certificate) certificate;
        } else {
            throw new IllegalArgumentException("Invalid certificate file");
        }
    }

    private static void storeServerCertificates(QuicClientConnection quicConnection, String serverCertificatesFile) throws IOException {
        List<X509Certificate> serverCertificateChain = quicConnection.getServerCertificateChain();
        if (!serverCertificatesFile.endsWith(".pem")) {
            serverCertificatesFile += ".pem";
        }
        PrintStream out = new PrintStream(new FileOutputStream(new File(serverCertificatesFile)));
        for (X509Certificate cert : serverCertificateChain) {
            out.println("-----BEGIN CERTIFICATE-----");
            try {
                out.print(new String(Base64.getMimeEncoder().encode(cert.getEncoded())));
            } catch (CertificateEncodingException e) {
                throw new IOException(e.getMessage());
            }
            out.println("\n-----END CERTIFICATE-----");
            out.println("\n");
        }
        out.close();
    }

    private static void storeNewSessionTickets(QuicClientConnection quicConnection, String baseFilename) {
        if (quicConnection.getNewSessionTickets().isEmpty()) {
            // Wait a little, receiver thread might still be busy processing messages.
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }

            if (quicConnection.getNewSessionTickets().isEmpty()) {
                System.out.println("There are new new session tickets to store.");
            }
        }

        quicConnection.getNewSessionTickets().stream().forEach(ticket -> storeNewSessionTicket(ticket, baseFilename));
    }

    private static void storeNewSessionTicket(NewSessionTicket ticket, String baseFilename) {
        int maxFiles = 100;
        File savedSessionTicket = new File(baseFilename + ".bin");
        int i = 1;
        while (i <= maxFiles && savedSessionTicket.exists()) {
            savedSessionTicket = new File(baseFilename + i + ".bin");
            i++;
        }
        if (i > maxFiles) {
            System.out.println("Cannot store ticket: too many files with base name '" + baseFilename + "' already exist.");
            return;
        }
        try {
            Files.write(savedSessionTicket.toPath(), ticket.serialize(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            System.err.println("Saving new session ticket failed: " + e);
        }
    }

    public static void doHttp09Request(QuicConnection quicConnection, String requestPath, String outputFile) throws IOException {
        doHttp09Request(quicConnection, requestPath, null, outputFile);
    }

    public static void doHttp09Request(QuicConnection quicConnection, String requestPath, QuicStream httpStream, String outputFile) throws IOException {
        if (httpStream == null) {
            boolean bidirectional = true;
            httpStream = quicConnection.createStream(bidirectional);
            httpStream.getOutputStream().write(("GET " + requestPath + "\r\n").getBytes());
            httpStream.getOutputStream().close();
        }

        if (outputFile != null) {
            FileOutputStream out;
            if (new File(outputFile).isDirectory()) {
                String fileName = requestPath;
                if (fileName.equals("/")) {
                    fileName = "index";
                }
                out = new FileOutputStream(new File(outputFile, fileName));
            } else {
                out = new FileOutputStream(outputFile);
            }
            IOUtils.copy(httpStream.getInputStream(), out);
            //httpStream.getInputStream().transferTo(out);
        } else {
            // Wait a little to let logger catch up, so output is printed nicely after all the handshake logging....
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }

            BufferedReader input = new BufferedReader(new InputStreamReader(httpStream.getInputStream()));
            String line;
            System.out.println("Server returns: ");
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
        }
    }

}
